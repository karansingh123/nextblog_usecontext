import NoteState from '../src/components/context/notes/NoteState';
import '../styles/global.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../src/containers/header";

export default function App({ Component, pageProps }) {
  return (
    <>
      <NoteState>
      <Header />
        <Component {...pageProps} />
      </NoteState>
    </>
  )
}
