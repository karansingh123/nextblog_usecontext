import Layout from "../src/components/layout";

export default function App() {
  return (
    <div className="container">

      <Layout />
    </div>
  );
}
