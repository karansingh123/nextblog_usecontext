import React, { useContext, useEffect, useState } from "react";
import { Form, Navbar, Nav, Container, NavDropdown } from "react-bootstrap";
// import styles from "./layout.module.css";
import { useRouter } from "next/router";
import Notecontext from "../components/context/notes/NoteContext";
import styles from "./layout.module.scss";

export default function Header(props) {
  const { selectedColor, changeColor, mode, toggleMode } = useContext(Notecontext);
  const containerBg = {
    backgroundColor: mode === "dark" ? "black" : "white",
  };
  const containerColor = { color: mode === "dark" ? "white" : "black" };

  const router = useRouter();
  const [storedUser, setStoredUser] = useState(true);
  const [formData, setFormData] = useState([]);
  useEffect(() => {
    setStoredUser(JSON.parse(localStorage.getItem("user")));
    console.log(storedUser, "aaaa");
  }, []);
  const handleClick = () => {
    const setFormData = {
      ...storedUser,
      islogin: "false",
    };
    localStorage.setItem("user", JSON.stringify(setFormData));
    router.push("/login");
  };

  return (
    <Navbar
      expand="lg"
      fixed="top"
      style={{ backgroundColor: mode === "dark" ? "#759CA0" : "#aaeff59d" }}
      className={`${props.mode} bg-${props.mode} ${styles.navbar}`}
    >
      <Container className="d-flex" style={containerColor}>
        <Navbar.Brand className="ms-5" href="/" style={containerColor}>
          {" "}
          My First Component
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav w-50" />
        <Navbar.Collapse id="basic-navbar-nav">
         
        </Navbar.Collapse>
        <Form.Check
          className=""
          onClick={toggleMode}
          type="switch"
          id="custom-switch"
          label="Dark Mode"
        />
      </Container>

      <NavDropdown
        className="me-5 p-0"
        title="Login/Signup"
        id="basic-nav-dropdown"
        style={containerColor}
      >
        <div style={containerBg}>
          {storedUser?.islogin !== "true" ? (
            <div>
              <Nav.Link href="/login" style={containerColor}>
                Login
              </Nav.Link>
              <Nav.Link href="/signup" style={containerColor}>
                Signup
              </Nav.Link>
            </div>
          ) : (
            <div>
              <Nav.Link href="/signup" style={containerColor}>
                Wellcome :{storedUser?.firstname}
              </Nav.Link>
              <Nav.Link onClick={() => handleClick} style={containerColor}>
                Logout
              </Nav.Link>
            </div>
          )}
        </div>
      </NavDropdown>
    </Navbar>
  );
}
