import { useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import { Container } from "react-bootstrap";
import Link from "next/link";
import { useRouter } from "next/router";
import Header from "../header";
import Notecontext from "../../components/context/notes/NoteContext";

const Signup = (props) => {
  const { mode, toggleMode } = useContext(Notecontext);
  const containerBg = {
    backgroundColor: mode === "dark" ? "black" : "white",
  };
  const containerColor = { color: mode === "dark" ? "white" : "black" };
  const router = useRouter();

  const [formData, setFormData] = useState({
    firstname: "",
    lastname: "",
    email: "",
    password: "",
    gender: "",
    islogin: "false",
  });
  // console.log(formData, "first")
  const handleInput = (e) => {
    const { name, value } = e.target;
    // console.log(e.target.value)
    setFormData({ ...formData, [name]: value });
  };
  const handlesubmit = (e) => {
    e.preventDefault();
    console.log(formData.email, formData.password);
    if (
      !formData.firstname ||
      !formData.lastname ||
      !formData.email ||
      !formData.password ||
      !formData.gender
    ) {
      alert("Please fill in all the fields");
      return;
    }
    localStorage.setItem("user", JSON.stringify(formData));
    router.push("/login");
  };
  return (
    <>
      <Header />
      <Container className="mt-5" style={containerColor}>
        <Link className=" d-flex justify-content-end" href="/home">
          {" "}
          <Button variant="info">Home</Button>
        </Link>
        <h2 className=" d-flex justify-content-center mb-4">
          Please register your account
        </h2>
        <Form onSubmit={handlesubmit}>
          <Row>
            <Col>
              <Form.Group as={Col}>
                <Form.Label>First name</Form.Label>
                <Form.Control
                  value={formData?.fname}
                  name="firstname"
                  onChange={handleInput}
                  placeholder="First name"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group as={Col}>
                <Form.Label>Last name</Form.Label>
                <Form.Control
                  value={formData.lname}
                  name="lastname"
                  onChange={handleInput}
                  placeholder="Last name"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row className="mb-3">
            <Form.Group as={Col}>
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={formData.email}
                name="email"
                onChange={handleInput}
                placeholder="Enter email"
              />
            </Form.Group>
          </Row>
          <Row>
            <Form.Group as={Col}>
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={formData.password}
                name="password"
                onChange={handleInput}
                type="password"
                placeholder="Password"
              />
            </Form.Group>
          </Row>
          <Row>
            <Col>
              <Form.Group as={Col}>
                <Form.Label>Gender</Form.Label>
                <Form.Control
                  value={formData.gender}
                  name="gender"
                  onChange={handleInput}
                  placeholder="Gender"
                />
              </Form.Group>
            </Col>
          </Row>
          <Button className="mt-3" variant="primary" type="submit">
            Submit
          </Button>
        </Form>
        <h6 className="pt-3">
          If already registered please :
          <Link className="text-decoration-none" href="/login">
            Login
          </Link>
        </h6>
      </Container>
    </>
  );
};

export default Signup;
