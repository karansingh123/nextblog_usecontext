import React, { useContext, useEffect, useState } from 'react'
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Link from "next/link";
import { useRouter } from 'next/router';
import Notecontext from '../../components/context/notes/NoteContext';
import Header from '../header';

export default function LOGIN() {
    const { mode, toggleMode } = useContext(Notecontext)
    const containerBg = {
        backgroundColor: mode === "dark" ? "black" : "white",
    }
    const containerColor = { color: mode === "dark" ? "white" : "black", }

    const router = useRouter();
    const [formData, setFormData] = useState({
        email: '',
        password: '',
    });
    console.log(formData, "1111")
    useEffect(() => {
        const newUser = JSON.parse(localStorage.getItem('user'));
        if (newUser) {
            setFormData({
                ...formData,
                email: newUser.email,
                password: newUser.password,
            });
        }
        console.log(newUser, "aaaa")

    }, []);
    // console.log(newUser, "2222")
    const handleInput = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };
    const handlesubmit = (e) => {
        e.preventDefault();

        const newUser = JSON.parse(localStorage.getItem('user'));
        if (newUser) {
            if (newUser.email === formData.email && newUser.password === formData.password) {
                const updatedFormData = {
                    ...newUser,
                    islogin: 'true',
                };
                console.log(updatedFormData, "333")
                localStorage.setItem('user', JSON.stringify(updatedFormData));
                router.push("home");
                // document.getElementById("loginlink").style.display = "none"


            } else {
                alert("Email or password is incorrect");
            }
        } else {
            alert("User not found. Please register first.");
        }
    };
    return (
        <div>
            <Header />
            <Container className='mt-5' style={containerColor} >
                <Link className=' d-flex justify-content-end' href="/home"> <Button variant="info">Home</Button></Link>
                <h2 className=' d-flex justify-content-center mb-4'>Please enter your account</h2>
                <Form onSubmit={handlesubmit} >
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="" value={formData.email} name='email' onChange={handleInput} placeholder="Enter email" />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={formData.password} name='password' onChange={handleInput} placeholder="Password" />
                    </Form.Group>

                    <Button className='mt-3' variant="primary" type="submit" >
                        Submit
                    </Button>
                </Form>
                <h6 className="pt-3">
                    If not register please :
                    <Link className="text-decoration-none" href="/signup">Register</Link>
                </h6>
            </Container>
        </div>
    )
}
