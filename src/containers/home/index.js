import Link from "next/link";
import React, { useContext, useState } from "react";
import { Button } from "react-bootstrap";
import Notecontext from "../../components/context/notes/NoteContext";

const TextForm = (props) => {
  const { mode, toggleMode } = useContext(Notecontext)
  const containerColor = {
    color: mode === "dark" ? "white" : "black",
  };
const containerBg = {backgroundColor: mode === "dark" ? "black" : "white",}
  const [text, setText] = useState("");
  // console.log(text);
  const handleOnChange = (event) => {
    setText(event.target.value);
  };
  const handalUpCase = () => {
    let newText = text.toUpperCase();
    setText(newText);
  };
  const handalLoCase = () => {
    let newText = text.toLowerCase();
    setText(newText);
  };
  const handalRexts = () => {
    let newText = text.split('   ');
    setText(newText.join(''));

  }
  const handalCopy = () => {
    navigator.clipboard.writeText(text);
  };
  const handalClear = () => {
    let newText = "";
    setText(newText);
  };
  return (
    <>
      <div
        className="container"
        style={containerColor}
      >
        <h2 className="mt-5">{props.heading}</h2>
        <textarea
          style={{
            backgroundColor: mode === "dark" ? "#2A2E32" : "white",
            color: mode === "dark" ? "white" : "black",
          }}
          rows={5}
          className="form-control mt-3"
          id="myBox"
          onChange={handleOnChange}
          value={text}
        ></textarea>
        <div className="mt-3">
          <Button className="me-2" onClick={handalUpCase} variant="success">
            Upper Case
          </Button>
          <Button className="me-2" onClick={handalLoCase} variant="warning">
            Lower Case
          </Button>
          <Button className="me-2" onClick={handalRexts} variant="info">
            RemoveExtSpc
          </Button>
          <Button className="me-2" onClick={handalCopy} variant="primary">
            Copy Text
          </Button>
          <Button className="me-2" onClick={handalClear} variant="danger">
            Clear
          </Button>
        </div>
      </div>
      <div
        className="container mt-4"
        style={containerColor}
      >
        <h2 >Your text summary </h2>
        <p>
          {text.split(" ").filter((element) => { return element.length !== 0 }).length} words and {text.length} characters
        </p>
        <p>{0.008 * text.split(" ").filter((element) => { return element.length !== 0 }).length} Minuts to read</p>
        <div className="mt-4">
          <h3>Preview</h3>
          <p>{text.length > 0 ? text : "Nothing to preview!"}</p>
        </div>
      </div>

    </>
  );
};
export default TextForm;
