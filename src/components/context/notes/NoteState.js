import React, { useEffect, useState } from "react";

import Notecontext from "./NoteContext";

const NoteState = (props) => {

    const toggleMode = () => {
        if (mode === "light") {
            setMode("dark");
            document.body.style.backgroundColor = "black";
        } else {
            setMode("light");
            document.body.style.backgroundColor = "white";
        }
    };
    const [mode, setMode] = useState("light");


    const [selectedColor, setSelectedColor] = useState('');

    useEffect(() => {
        document.body.style.backgroundColor = selectedColor;
    }, [selectedColor]);

    const changeColor = (color) => {
        setSelectedColor(color);
    };
    return (
        <Notecontext.Provider value={{ mode, toggleMode, changeColor, selectedColor }}>
            {props.children}
        </Notecontext.Provider>
    );
};

export default NoteState;
